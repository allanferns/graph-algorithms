//INSERTIONSORT
//Start from left to right. Mark the first element as sorted. Then check the next element to it's right.
//If it is smaller, swap the two elements.
//Keep swappingn it with elements to it's left if they are larger than the unsorted element.
//Do this until the whole array is sorted

async function insertsort(arr) {
    var mark = 0;
    if (arr.length == 1) { //Edge case if the element length is just 1
        return arr;
    } else if (arr.length == 2) { // A second edge case if the element length is just 2
        if (arr[0] > arr[1]) {
            swap(arr, 0, 1);
            updateArray3(0, 1);
            await sleep(1);
        }
    } else {
        mark = 2; // we start looking at the 3rd element and manually sort the first two elements.
        if (arr[0] > arr[1]) { //the manual sorting part
            swap(arr, 0, 1);
            updateArray3(0, 1);
            await sleep(1);

        }
        while (mark != arr.length) { //We will loop until the mark reaches the other end of the array
            if (arr[mark] > arr[mark - 1]) { //If we are considering two elements which are already sorted
                mark++;
            } else { //In the case where the two elements which we are considering haven't been sorted.
                if (arr[mark] <= arr[0]) {
                    insert(arr, mark, 0);
                    updateArray3(0, mark);
                    await sleep(1);
                } else {
                    for (let i = mark - 1; arr[mark] <= arr[i]; i--) {
                        if (arr[i - 1] < arr[mark]) {
                            insert(arr, mark, i);
                            updateArray3(i, mark);
                            await sleep(1);
                        }
                    }
                }

                mark++;
                updateArray3(0, mark);
                await sleep(1);
            }
        }
    }
    displayarray(globalarray);
    return arr;
}

function insert(arr, mark, ins) {
    var temp = arr[mark];
    for (let i = mark; i > ins; i--) { //Shift the whole array.
        arr[i] = arr[i - 1];
    }
    arr[ins] = temp; //swap the elements
}

async function specinsertsort(){
    if (sorted == true) { // check this one
        generatearray();
    }
    if (randomized == false) {
        return;
    }
    if (currentlysorting == true) {
        return;
    }
    currentlysorting = true;
    await insertsort(globalarray);
    currentlysorting = false;
    sorted = true;
}