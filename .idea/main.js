var c = document.getElementById('myCanvas');
var ctx = c.getContext('2d');
ctx.fillStyle = 'red';

var canvasHeight = document.getElementById('myCanvas').height;
console.log("canvas height is " + canvasHeight);
var globalarray = [];
globalarray.length = 950;

currentlysorting = false;
sorted = false;
randomized = false;

var leftmarker;
var rightmarker;
var travmarker;

function generatearray() {
    if (currentlysorting == true) {
        return false;
    }
    ctx.clearRect(0, 0, 1000, 500);
    for (let i = 0; i < globalarray.length; i++) {
        globalarray[i] = Math.floor(Math.random() * 450 + 1);

        ctx.fillRect(25 + i, canvasHeight - 25, 1, -globalarray[i]);
    }
    randomized = true;
    sorted = false;
    return false;
}

function updateArray() {
    ctx.clearRect(0, 0, 1000, 500);
    for (let i = 0; i < globalarray.length; i++) {
        ctx.fillRect(25 + i, canvasHeight - 25, 1, -globalarray[i]);
    }
}

function updateArray2(a, b, c) {
    ctx.clearRect(0, 0, 1000, 500);
    for (let i = 0; i < globalarray.length; i++) {
        if (i == a || i == b || i == c) {
            ctx.fillStyle = 'green';
            ctx.fillRect(25 + i, canvasHeight - 25, 1, -globalarray[i]);
            ctx.fillStyle = 'red';
        } else {
            ctx.fillRect(25 + i, canvasHeight - 25, 1, -globalarray[i]);
        }

    }
}

function updateArray3(a, b) {
    ctx.clearRect(0, 0, 1000, 500);
    for (let i = 0; i < globalarray.length; i++) {
        if (i == a || i == b) {
            ctx.fillStyle = 'green';
            ctx.fillRect(25 + i, canvasHeight - 25, 1, -globalarray[i]);
            ctx.fillStyle = 'red';
        } else {
            ctx.fillRect(25 + i, canvasHeight - 25, 1, -globalarray[i]);
        }

    }
}

function swap(array, a, b) {
    if (a == b) {
        return;
    }
    var c = array[a];
    array[a] = array[b];
    array[b] = c;
}

function displayarray(arr) {
    for (let i = 0; i < arr.length; i++) {
        console.log(arr[i]);
    }
}

function sleep(ms) {
    return new Promise((accept) => {
        setTimeout(() => {
            accept();
        }, ms);
    })
}

async function sleeptest() {
    for (let index = 0; index < 10; index++) {
        console.log(index);
        await sleep(1000);

    }
}

alert("Click on generate array before first time use. After the first use, arrays will automatically be generated");





