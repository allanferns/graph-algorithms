//MERGESORT

//The speed of this algorithm is nlog(n)
//there are log2(n) levels
//The merge subroutine works in linear time. there is a single while loop.
//Hence the speed of the overall algorithm is nlog(n)
async function merge(arr, left, mid, right) {
    temp = [];
    temp.length = arr.length;
    for (let i = 0; i < arr.length; i++) {
        temp[i] = arr[i];
    }
    if (right - left == 1 && arr[right] < arr[left]) {
        swap(arr, right, left);
        return arr;
    } else if (right - left == 1 && arr[left] < arr[right]) {
        return arr;
    }
    var marker1 = left; //Marker for the left sorted array
    var marker2 = mid + 1; //Marker for the right sorted array
    var marker3 = left; //Marker for the merged array
    while (marker1 <= mid && marker2 <= right) { //We will merge while both markers have not reached their end points.
        if (arr[marker1] <= arr[marker2]) {
            temp[marker3++] = arr[marker1++];
        }
        if (arr[marker2] <= arr[marker1]) {
            temp[marker3++] = arr[marker2++];
        }
        // marker1 needs many more attempts before breaking
    }
    if (marker1 >= mid) { //If marker1 is at the end point, but marker2 isn't. Fill the rest of the merged array with the part that hasn't been merked yet
        while (marker2 <= right) {
            temp[marker3++] = arr[marker2++];
        }
    }
    if (marker2 >= right) { //If marker2 is at the end point, but marker1 isn't. Fill the rest of the array with unmerged part
        while (marker1 <= mid) {
            temp[marker3++] = arr[marker1++];
        }
    }
    for (let i = 0; i < arr.length; i++) {
        arr[i] = temp[i];
    }

    return arr;
}

async function recmergesort(arr, left, right) {
    if (left >= right) { //The end condition of recursion.
        return arr;
    }
    var midpoint = Math.floor((left + right) / 2); //Dividing the array in 2 every time. The number of levels will be Log2(arr.length)
    await recmergesort(arr, left, midpoint); //Merge theleft part
    await recmergesort(arr, midpoint + 1, right); //merge the right part
    await merge(arr, left, midpoint, right); // The previous 2 functions are recursive and after the recursive part, we will merge them in linear time
    updateArray2(left, midpoint, right); //Updating the display array
    await sleep(1); //pause to visualise
    return arr;
}

function mergesort(arr) {
    return recmergesort(arr, 0, arr.length - 1); //calling the recursive merge sort in a non recursive method
}

async function specmergesort() { //The function which is called when the button is pressed
    if (sorted == true) { // check this one
        generatearray();
    }
    if (randomized == false) { //WORKS
        return;
    }
    if (currentlysorting == true) {
        return;
    }
    currentlysorting = true;
    await mergesort(globalarray);
    currentlysorting = false;
    sorted = true;//After the function, we update the flags
}