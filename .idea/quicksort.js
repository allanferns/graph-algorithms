//QUICKSORT

function selectpartitionelement(left, right) {
    var randomnum = Math.floor((Math.random() * (right - left) + left));
    return randomnum;
}

async function partition(arr, left, right) {//Consider part of the array between left and right
    if (left == right) {
        return;//Terminate recursion
    }
    var lt;//the left marker
    var current;//the traversal marker
    var gt;//the right marker
    var partitionelement = arr[selectpartitionelement(left, right)];//Randomised quicksort. The partitionelement is randomised
    //A random index is selected and then the element at that index becomes the partitionelement
    lt = left;
    gt = right;
    current = left;

    leftmarker = lt;//Global variables important to recursion. Declared in the main.js
    rightmarker = gt;
    travmarker = current;
    do {
        if (arr[current] < partitionelement) {
            swap(arr, lt++, current++);//If the current element is lower than the PE, swap it with the left marker
            updateArray3(lt, gt);

        } else if (arr[current] > partitionelement) {
            swap(arr, gt--, current); //If it is larger than the partition element, swap it with the right marker
            updateArray3(lt, gt);

        } else if (arr[current] == partitionelement) {
            current++;//The elements equal to the partition element will clump up in the middle as any larger or smaller elements get swapped out
        }
        
        leftmarker = lt;
        rightmarker = gt;
        travmarker = current;
        await sleep(0);

    } while (current <= gt);
                            
}

async function recquicksort(arr, left, right) {

    if (left >= right) {
        return;
    } else {

        await partition(arr, left, right);//Partition element was selected
        await recquicksort(arr, left, leftmarker - 1); //recursively sort left and right parts of the array
        await recquicksort(arr, rightmarker + 1, right);
    }
    updateArray(globalarray);
    await sleep(1);
}

async function quicksort(arr) { // the non recursive function which calls the recursive part
    leftmarker = 20; // one of many indices which would work
    await recquicksort(arr, 0, arr.length - 1);
    displayarray(arr);
}

async function specquicksort() { //the function called when button is pressed.
    if (sorted == true) { // Generate a new array before it can start sorting because the array has already been sorted.
        generatearray();
    }
    if (randomized == false) {
        return;
    }
    if (currentlysorting == true) { //flags to stop the sort if others are being sorted.
        return;
    }
    currentlysorting = true;
    await quicksort(globalarray); //The action sort.
    currentlysorting = false; //update the flags after the sort has ended
    sorted = true;
}