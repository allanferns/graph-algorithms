async function bubsort(arr) {
    var N = arr.length;
    var j = arr.length;
    while (j >= 1) {
        for (let i = 0; i < j - 1; i++) { //for loop inside a w hile loop so the time complexity is quadratic.
            compare(arr, i, i + 1); //consecutive elements are constantly compared and sorted.
                                        //This way, the largest number is 'bubbled' to the top.
        }
        j--;
        updateArray(globalarray); //updating the array which is finally displayed on the canvas.
        await sleep(10); //pause so we can see what is going on
    }
    return arr;
}


function compare(arr, i, j) { // i < j
    if (arr[i] > arr[j]) {
        swap(arr, i, j);
    }
}

async function specbubblesort(){ //the function which is called when the button is pressed.
    if (sorted == true) { // Generate an array if the current array has already been sorted.
        generatearray();
    }
    if (randomized == false) {
        return;
    }
    if (currentlysorting == true) {
        return;
    }
    currentlysorting = true;
    await bubsort(globalarray); //Calling the sorting function
    currentlysorting = false; //Update the flags at the end of the sort.
    sorted = true;
}


