

class Graph {

    mazeGenerated; //flags
    searching;
    MAX_VERTS; //Number of vertices

    vertexList = new Array();//All the arrays which need to be created
    adjMat;
    openset = new Array();
    closedset = new Array();
    mazeDFStack = new Array();
    erasedEdges = new Array();
    wallList = new Array();

    constructor() {
        this.searching = false;
    };


    displayadjmat() {
        console.log(this.adjMat);
    }
    getNumberofVertices() {
        return this.vertexList.length;
    }


    //Unused function
    displayVertex(v) {
        console.log(v.label);
    }
    //Unused function
    displayVertexList() {
        console.log(this.vertexList);
    }
    //Setup function
    addVertex(v) {
        this.vertexList.push(v);
    }
    //////////------------------setup function---------------///////////////
    addEdge(v1, v2) {
        this.adjMat[this.getVertexidnex(v1)][this.getVertexidnex(v2)] = 1; //the undirected edge is then created in the adj matrix
        this.adjMat[this.getVertexidnex(v2)][this.getVertexidnex(v1)] = 1; //Both directions.
    }

    initializeadjMat() {
        //create 2d matrix for adjMat[][]
        this.MAX_VERTS = this.getNumberofVertices();
        this.adjMat = new Array(this.MAX_VERTS);
        for (let i = 0; i < this.MAX_VERTS; i++) {
            this.adjMat[i] = new Array(this.MAX_VERTS); //Creating the 2d matrix.
        }
        for (let i = 0; i < this.MAX_VERTS; i++) {
            for (let j = 0; j < this.MAX_VERTS; j++) {
                this.adjMat[i][j] = 0; // initialising the whole matrix to 0
            }
        }
    }
    ////////// ------------ USED EXTERNALLY MOSTLY --------------//////////

    resetErasedEdges(){ //reset the array
        this.erasedEdges = new Array();
    }

    drawVertices() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        //this.drawEdges(); This can be uncommended out to show the edges, but it will interfere with the maze generator
        //it is drawn before the vertices so it isn't drawn over them
        for (let i = 0; i < this.MAX_VERTS; i++) {
            this.vertexList[i].draw();
        }

    }
    drawEdges() { //Unused function right now
        ctx.beginPath();
        for (let i = 0; i < this.adjMat.length; i++) {
            for (let j = 0; j < this.adjMat.length; j++) {
                if (this.adjMat[i][j] == 1) {
                    ctx.moveTo(this.vertexList[i].edgex, this.vertexList[i].edgey);//from centre of one vertex
                    ctx.lineTo(this.vertexList[j].edgex, this.vertexList[j].edgey);//to the centre of the second vertex
                }
            }
        }
        ctx.stroke(); //end path
    }

    disableVertex(vertex) {
        if (vertex.startVertex == true || vertex.endVertex == true) {
            return;
        }
        vertex.disabled = true;
        for (let i = 0; i < this.vertexList.length; i++) {
            this.adjMat[this.getVertexidnex(vertex)][i] = 0; //To disable a vertex, just disable the edges
            this.adjMat[i][this.getVertexidnex(vertex)] = 0; //By setting it to 0 in adj matrix
        }
    }

    async resetgraph() {
        for (let i = 0; i < this.vertexList.length; i++) { //Loop through the array for every vertex
            this.vertexList[i].wasVisited = false; //reset the characteristics of the vertex
            this.vertexList[i].probing = false;
            this.vertexList[i].disabled = false;
        }
        this.drawVertices();
    }
    ///////////////////////////-------------Basic functions for most of the searches--------------///////////////////

    getVertexidnex(v) {
        for (let i = 0; i < this.MAX_VERTS; i++) { //returns the vertex index of a vertex
            if (this.vertexList[i] == v) {
                return i;
            }
        }
    }



    getunvisitedNeighbour(vertex) { // check neighbour AND if they are visited. return the neighbour
        //get the i number of vertex
        for (let i = 0; i < this.MAX_VERTS; i++) {
            if (this.adjMat[this.getVertexidnex(vertex)][i] == 1 && this.vertexList[i].wasVisited == false) {
                return this.vertexList[i];
            }
        }
    }
    //boolean
    unvisitedneighbours(vertex) { //Scan the whole matrix for neighbours AND if they are are unvisited neighbours. return boolean
        for (let i = 0; i < this.MAX_VERTS; i++) {
            if (this.adjMat[this.getVertexidnex(vertex)][i] == 1 && this.vertexList[i].wasVisited == false) {
                return true;
            }
        }
        return false;
    }

    ///////////-------------Searches---------////////////

    async iterativedfs(vertex0) {
        this.searching = true; //turn on the flag
        //visit start point
        //push start point on to stack
        //get unvisited neighnour of start point
        //keep traversing until there are no unvisited neighbours
        //keep popping vertices until you reach a vertex with unvisited neighbours
        //end when the start vertex has been popped
        let vertexstack = new Array(); //create the stack
        let travVertex = vertex0; //the start vertex
        let dfstack = new Array(); //create the search stack
        vertexstack.push(travVertex);
        dfstack.push(travVertex);
        travVertex.wasVisited = true; //mark the vertex as visited
        travVertex.probing = true; //for display purposes
        this.drawVertices(); //update the grid with the new colour of the start vertex
        this.eraseAllremovedEdges(); //remove the edges which were removed by the maze generator
        travVertex.probing = false;
        while (vertexstack.length != 0) { //continue until the stack is empty
            await sleep(1); //pause for display purposes
            if (this.unvisitedneighbours(travVertex) == true) { //if it has unvisited neighbours
                travVertex = this.getunvisitedNeighbour(travVertex); //move to the unvisited neighbour
                vertexstack.push(travVertex);
                dfstack.push(travVertex); //add that vertex to the stacks
                travVertex.wasVisited = true; //mark it
                travVertex.probing = true; //mark it in the drawing
                this.drawVertices(); //update the whole drawing
                this.eraseAllremovedEdges(); //remove all the edges MG removed edges after every drawing
                travVertex.probing = false;
            } else { //if it doesn't have any neighbours
                vertexstack.pop(); //remove it from the stack
                travVertex = vertexstack[vertexstack.length - 1]; //get the next vertex from the stack and continue
                if (vertexstack.length == 0) { //Trying to execute the next few lines will crash if there are no vertices in the stack
                    break;
                }
                travVertex.probing = true;
                this.drawVertices();
                this.eraseAllremovedEdges();
                travVertex.probing = false;
            }
        }

        console.log(dfstack);
        this.searching = false; //the flag to indicate that other searches can now begin
    }

    async iterativeBFS(vertex0) {
        this.searching = true;//Turn on the flag
        let travVertex = vertex0; //start with the starting vertex
        let travNeighbour;
        let traversalQueue = new Array(); //construct the queues
        let neighbourQueue = new Array();
        travVertex.wasVisited = true; //mark the travVertex as visited
        traversalQueue.push(travVertex); //push it into the queue

        do {
            while (this.unvisitedneighbours(travVertex) == true) { //Only backtrack when the vertex has no unvisited neighbours
                await sleep(1);
                travNeighbour = this.getunvisitedNeighbour(travVertex); //move to the vertex neighbour which has not been visited
                travNeighbour.wasVisited = true; //mark it visited
                neighbourQueue.push(travNeighbour); //push this vertex into the queu
                travNeighbour.probing = true; //Mark it for the drawing
                this.drawVertices(); //Draw the whole grid with the newly marked vertices
                this.eraseAllremovedEdges() //remove any edges which were disabled by the maze generator
                travNeighbour.probing = false;

            }
            travVertex = neighbourQueue.shift(); //We backtrack here because the travVertex has no unvisited neighbours
            traversalQueue.push(travVertex)//Selecting the new vertex
        } while (neighbourQueue.length != 0); //loop until the stack is empty
        console.log(traversalQueue);
        this.searching = false; //indicate that other searches can now begin
    }


    async shortestpath(vertex0, vertex1) {
        this.searching = true; //Indicate that other searches are suspended
        let travVertex = vertex0; //The starting vertex
        vertex1.endVertex = true;  //for drawing purposes
        let travNeighbour; //declarations
        let neighbourQueue = new Array();

        while (travVertex != vertex1) { //Loop until we have reached the endpoint
            while (this.unvisitedneighbours(travVertex) == true) {
                await sleep(1);
                travNeighbour = this.getunvisitedNeighbour(travVertex); //move to the unvisited neighbour
                travNeighbour.previous = travVertex; //To trace back later from the final point to the starting point
                //You will notice that the path is drawn backwards, from the endpoint to the starting point

                travNeighbour.wasVisited = true; //mark it visited so we don't visit it again
                neighbourQueue.push(travNeighbour); //add it to the queue
                travNeighbour.probing = true; //for drawing purposes
                this.drawVertices();
                this.eraseAllremovedEdges(); //draw all the vertices and remove the edges which were removed by the maze generator

                travNeighbour.probing = false; //Only 1 vertex is probing at any point in time
                if (travNeighbour == vertex1) {
                    break;
                }
            }
            travVertex = neighbourQueue.shift(); //move on to the first vertex in the queue
            if (travNeighbour == vertex1) {
                break;
            }
        }

        //The shortest path is traced backwards here

        let pathVertex = vertex1; //start at the end point
        pathVertex.spVertex = true; //mark is as the path vertex so we can draw it later
        while (pathVertex != vertex0) { // return from the finish point to the start point using .previous's
            await sleep(1); //pause for the display
            pathVertex = pathVertex.previous; //iterate to the previous vertex
            console.log(pathVertex);
            pathVertex.spVertex = true;
            this.drawVertices();
            this.eraseAllremovedEdges(); //draw the whole grid and update it with the new path vertex. Have to also remove the MG removed-edges
        }
        this.searching = false; //indicate other searches can begin
    }

    //////-------A* search functions only------/////////
/*
* slider ranges from 1 to 10
* the total value should always = 2
* middle of slider should be 1 for each
* */
    getCostoftravel(v1, v2) { //v1 would be the current vertex v2 would be the destination
        let absy = Math.abs(v2.ycoord - v1.ycoord);
        let absx = Math.abs(v2.xcoord - v1.xcoord);

        return absx + absy;
    }
    slidervalue() { // between 0 and 2
        let sliderval = document.getElementById('bias').value;
        console.log(sliderval);
        return sliderval;
    }

    updateOpenSet(vertex1) {//need neighbours of all the closed set members. // do not put vertices from the closed set into the open set
        for (let i = 0; i < this.closedset.length; i++) {
            for (let j = 0; j < this.MAX_VERTS; j++) {
                if (this.adjMat[j][this.getVertexidnex(this.closedset[i])] == 1 && this.vertexList[j].isinClosedSet == false && this.vertexList[j].isinOpenSet == false) {
                    this.vertexList[j].previous = this.closedset[i];
                    this.vertexList[j].isinOpenSet = true; //mark it for the drawing and for selection
                    this.vertexList[j].gVal = (i + 1) * 40;
                    this.vertexList[j].hVal = this.getCostoftravel(this.vertexList[j], vertex1);

                    this.vertexList[j].fVal = (10-this.slidervalue())*this.vertexList[j].gVal + this.slidervalue()*this.vertexList[j].hVal; //get the slider value in real time
                    this.openset.push(this.vertexList[j]);
                }
            }
        }
    }

    selectnextneighbour() {
        //get the vertex with the lowest f value from the open set
        let vertexIndex = 0;
        let travelCost = this.openset[0].fVal;
        for (let i = 1; i < this.openset.length; i++) {
            if (this.openset[i].fVal <= travelCost) {
                travelCost = this.openset[i].fVal;
                vertexIndex = i; //select the vertex with the lowest travel cost
            }
        }
        return this.openset[vertexIndex];
    }
    transferFromOpenSettoClosedSet(vertex) { //moving it from one set to the other
        for (let i = 0; i < this.openset.length; i++) {
            if (this.openset[i] == vertex && this.openset[i].isinClosedSet == false) {
                this.openset[i].isinOpenSet = false; //markings for the drawing
                this.openset[i].isinClosedSet = true;
                this.closedset.push(this.openset[i]);  //pushing it into the closed set
                this.openset.splice(i, 1); //removing it from the open set and reducing the size of the array.
                return;
            }
        }
    }

    async aStarSearch(vertex0, vertex1) {
        document.getElementById("sliderval").innerText = "Bias Value: " + (this.slidervalue()-5); // Set the bias value
        this.searching = true;
        //put vertex0 into the closed set. CLOSED SET IS FOR THE ACTUAL PATH ELEMENTS
        //generate f values for all the neighbours of the closed set members and put them all into the open set
        //select the lowest f value vertex in the open set and MOVE IT FROM THE OPEN SET TO THE CLOSED SET
        //generate f values for all the neighbours of the closed set members and put them all into the open set
        //select lowest f value vertex in the open set and move it from the open set to the closed set
        //repeat until vertex1 is in the closed set
        //the path is all the elements in the closed
        this.closedset.push(vertex0);
        this.updateOpenSet(vertex1);
        this.transferFromOpenSettoClosedSet(this.selectnextneighbour());

        while (vertex1.isinClosedSet == false) {
            this.updateOpenSet(vertex1);
            this.transferFromOpenSettoClosedSet(this.selectnextneighbour()); //get correct neighbour and put it into the closed set
            await sleep(0);
            this.drawVertices();
            this.eraseAllremovedEdges(); //update the drawing with vertices and the removed edges from maze generation
            if (this.openset.length == 0) { //When there is no way to the end point, we reset everything
                this.searching = false;
                this.openset = new Array();
                this.closedset = new Array();
                console.log("no solution");
                return;
            }
        }
        //drawing the path here. Draw from the end point to the start point
        let pathVertex = vertex1;
        pathVertex.apVertex = true;
        while (pathVertex != vertex0) {
            await sleep(0);
            pathVertex = pathVertex.previous; //backtracking to the start and marking the vertices along the way
            //console.log(pathVertex);
            pathVertex.apVertex = true; // for drawing purposes
            this.drawVertices();
            this.eraseAllremovedEdges();
        }
        this.openset = new Array(); //reset the sets for future searches. The sets are used by other searches/MG
        this.closedset = new Array();
        this.searching = false; //indicate that other searches can begin.
        console.log("ended");
    }

        ////////-----Primarily maze generator functions-------///////////

    selectRandomNeighbour() {
        let num;
        num = Math.floor(Math.random() * this.openset.length); // randomnumber within the range 0 - length
        return this.openset[num]; //returning a random number from the set
    }

    selectRandomNeighbour(v) {
        let neighbourSet = new Array();

        for (let i = 0; i < this.MAX_VERTS; i++) {
            if (this.adjMat[i][this.getVertexidnex(v)] == 1) { //adding all the neighbours to this set
                neighbourSet.push(this.vertexList[i]);
            }
        }
        let num = Math.floor(Math.random() * neighbourSet.length);
        return neighbourSet[num]; //random vertex from just the neighbour set

    }

    unvisitedVerticesExist() { //boolean to check if unvisited vertices exist
        for (let i = 0; i < this.vertexList.length; i++) {
            if (this.vertexList[i].wasVisited == false) { //checking every vertex. True if any exist
                return true;
            }
        }
    }
    eraseAllremovedEdges() {
        let marker = 0;
        while (marker < this.erasedEdges.length) {
            this.eraseEdge(this.erasedEdges[marker][0], this.erasedEdges[marker][1]); //erasing every edge inside the erasedEdges set
            //console.log("erasing");
            marker++;
        }
    }

    eraseEdge(v1, v2) { //Purely graphical representation for erasing the edges
        if (Math.abs(v1.xcoord - v2.xcoord) > v1.lengthSize || Math.abs(v1.ycoord - v2.ycoord) > v1.lengthSize) {
            console.log("Invalid edge selected for erasing");
            return;
        }
        if (v1.ycoord == v2.ycoord) { //if they are vertically aligned
            if (v1.xcoord < v2.xcoord) { //v1 is to the left of v2
                ctx.clearRect(v1.xcoord + v1.lengthSize - 1, v1.ycoord, 2, v1.lengthSize);
            } else if (v1.xcoord > v2.xcoord) { //v1 is to the right of v2
                ctx.clearRect(v2.xcoord + v1.lengthSize - 1, v1.ycoord, 2, v1.lengthSize);
            }
        }
        else if (v1.xcoord == v2.xcoord) {//if they are horizontally aligned

            if (v1.ycoord < v2.ycoord) { //v1 is on top of v2
                ctx.clearRect(v1.xcoord, v1.ycoord + v1.lengthSize - 1, v1.lengthSize, 2);
            } else if (v1.ycoord > v2.ycoord) { // v1 is below v2
                ctx.clearRect(v2.xcoord, v2.ycoord + v1.lengthSize - 1, v1.lengthSize, 2);
            }
        }
    }

    async dFSMazeGenerator(vertex0) {
        this.searching = true; //Flag to stop other searches from doing anything while this one is running
        let travVertex = vertex0; //start with vertex0
        vertex0.wasVisited = true; //mark the vertex as visited
        this.mazeDFStack.push(travVertex); //push it onto a stack

        while (this.unvisitedneighbours(travVertex)) {
            travVertex = this.selectRandomNeighbour(travVertex); //select a random neighbour of the traversal vertex
            //set it to be the new traversal vertex
            travVertex.wasVisited = true; //do the same things to it. mark it as visited and push it onto the stack
            this.mazeDFStack.push(travVertex);
            this.erasedEdges.push([this.mazeDFStack[this.mazeDFStack.length - 1], this.mazeDFStack[this.mazeDFStack.length - 2]]);
            //the edge between the 2 consecutive traversal vertices need to be added to the 'erased edges' array for future operations
            this.eraseEdge(this.mazeDFStack[this.mazeDFStack.length - 1], this.mazeDFStack[this.mazeDFStack.length - 2]);
            await sleep(0);
            while (this.unvisitedneighbours(travVertex) == false) { //backtracking part if there are no unvisited vertices
                this.mazeDFStack.pop();
                if (this.mazeDFStack.length == 0) {
                    break;
                }
                travVertex = this.mazeDFStack[this.mazeDFStack.length - 1];

            }

        }
        //empty closed and open sets;
        this.openset = [];
        this.closedset = [];

        //disable all the edges
        for (let i = 0; i < this.MAX_VERTS; i++) {
            for (let j = 0; j < this.MAX_VERTS; j++) {
                this.adjMat[i][j] = 0;
            }
        }

        //enable the edges which were erased. The erased edges actually have adges because that's the 'open wall'
        for (let i = 0; i < this.erasedEdges.length; i++) {
            this.addEdge(this.erasedEdges[i][0], this.erasedEdges[i][1]);
        }
        //reset visitations
        for (let i = 0; i < this.vertexList.length; i++) {
            this.vertexList[i].wasVisited = false;
        }
        console.log("finished maze generating");
        this.searching = false; //reset flags
        this.mazeGenerated = true;

    }


    /////////------Prims maze generator-----/////
    async primsMazeGenerator(vertex0) {
        /*
        SCRAPPED
        Start with a grid full of walls.
        Pick a cell, mark it as part of the maze. Add the walls of the cell to the wall list.
        While there are walls in the list:
            Pick a random wall from the list. If only one of the cells that the wall divides is visited, then:
            Make the wall a passage and mark the unvisited cell as part of the maze.
            dd the neighboring walls of the cell to the wall list.
            Remove the wall from the list.

        */
        //turn the whole grid into walls and add them all to the wall list
    }

}

