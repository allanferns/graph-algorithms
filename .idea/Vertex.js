var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");


class Vertex {
    label;
    wasVisited;
    xcoord;
    ycoord;
    edgex;
    edgey;
    lengthSize;
    disabled;
    probing;
    startVertex;
    endVertex
    previous;
    next;
    spVertex;
    fVal;
    hVal;
    gVal;
    isinClosedSet;
    isinOpenSet;
    isinWallList;
    apVertex;

    constructor(label, xcoord, ycoord) {
        this.label = label; //label should be a character value
        this.wasVisited = false;
        this.xcoord = xcoord;
        this.ycoord = ycoord;
        this.lengthSize = 30;
        this.edgex = xcoord + this.lengthSize / 2;
        this.edgey = ycoord + this.lengthSize / 2;
        this.disabled = false;
        this.probing = false;
        this.startVertex = false;
        this.endVertex = false;
        this.sPVertex = false;
        this.isinClosedSet = false;
        this.isinOpenSet = false;
        this.isinWallList = false;
        this.apVertex = false;
    }



    draw() { // The order is important here because of the returns.
        ctx.clearRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
        ctx.lineWidth = 2;
        ctx.strokeRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
        if (this.startVertex == true) {
            ctx.fillStyle = "rgba(255,255,0, 1)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }else if (this.endVertex == true) {
            ctx.fillStyle = "rgba(128,0,128, 1)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }else if (this.probing == true) {
            ctx.fillStyle = "rgba(255,0,0, 1)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }else if(this.apVertex == true){
            ctx.fillStyle = "rgba(0,128,128, 1)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        } else if (this.spVertex == true) {
            ctx.fillStyle = "rgba(128,128,0, 1)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }else if (this.isinClosedSet == true) {
            ctx.fillStyle = "rgba(0,255,0, 0.3)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }else if (this.isinOpenSet == true) {
            ctx.fillStyle = "rgba(255,165,0, 0.5)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }else if (this.wasVisited == true) {
            ctx.fillStyle = "rgba(255,165,0, 0.5)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }else if(this.isinWallList == true){
            ctx.fillStyle = "rgba(0,255,0, 0.3)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }else if (this.disabled == true) {
            ctx.fillStyle = "rgba(0,0,0, 0.5)";
            ctx.fillRect(this.xcoord, this.ycoord, this.lengthSize, this.lengthSize);
            return;
        }
    }
}
async function sleep(ms) {
    return new Promise((accept) => {
        setTimeout(() => {
            accept();
        }, ms);
    })
}

