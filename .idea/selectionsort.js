//Keep track of the first element in the unsorted part of the array
//Iterate through the rest of the array and check if there are any numbers smaller than the current minimum
//Mark the current minimum
//If the minimum is smaller than the first element in the unsorted part of the array,
//swap elements


async function selectionsort() { //Has to run async for the sleep function to work
    if (randomized == false) {
        return;
    }
    if (sorted == true) { //before the sorting can begin, a randomised array should be created.
        generatearray();
    }

    if (currentlysorting == false) { // If it is already sorting, don't start sorting again
        currentlysorting = true;
    } else {
        return;
    }
    var travindex1 = 0;
    var travindex2 = 0; //Initialising the two traversal indices
    mindex = travindex1;
    while (travindex1 < globalarray.length - 1) { //Searching through the whole array

        if (globalarray[travindex1] < globalarray[travindex2]) {
            mindex = travindex1;
        } else {
            mindex = travindex2;
        }
        for (let i = travindex2; i < globalarray.length - 1; i++) { //For each while loop, we search for the minimum value

            if (globalarray[i] < globalarray[mindex]) {
                mindex = i;
            }
        }
        swap(globalarray, mindex, travindex1); //We then swap the minimum value with the starting index.
        updateArray(); //Changing the array which will be displayed
        await sleep(1); //Pause between loops so we can see the change
        travindex1++;
        travindex2++; //for the next loop, we will start with one element to the right
    }
    currentlysorting = false;
    sorted = true; //Flags so we don't have multiple sorting algorithms running simultaneously
}