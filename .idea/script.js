var theGraph = new Graph();
var columns = 40; //Number of columns in the grid
var rows = 19;      //Number of rows in the grid
var selectedStartingVertex = false; //Flags, so we select the correct type of vertex
var selectedEndingVertex = false;
var hasbeenreset = false; //Flag to prevent the searches from interfering with eachother
var gap = 0; //gap between vertices when we draw them
var vertexLength = 30; // have to change it in Vertex class too if changed here
var vertexOffset =20; //padding in the canvas
var vertexgrid = new Array(columns); //10 columns
var mouseIsDown = false;

function getEndingVertex(){ //can be improved by simply storing the end vertex in a variable
    selectedEndingVertex = true;
    for (let i = 0; i < columns; i++) { //Scan the whole vertex grid in both dimensions in search for the ending vertex
        for (let j = 0; j < rows; j++) {
            if (vertexgrid[i][j].endVertex == true) {
                return vertexgrid[i][j]; //Return the vertex which is the ending vertex
            }
        }
    } //
}


function getstartingvertex() { //Can be improved by simply storing the start vertex in a variable
    selectedStartingVertex = true;
    for (let i = 0; i < columns; i++) { //scan the whole vertexgrid in both dimentions
        for (let j = 0; j < rows; j++) {
            if (vertexgrid[i][j].startVertex == true) {
                return vertexgrid[i][j]; //return the vertex which has been selected to start.
            }
        }
    }
}
function graphreset() { //the reset button calls this function. It resets all the flags and clears the necessary arrays.
    if (theGraph.searching == true) {
        console.log("already searching");
        return;
    }
    selectedStartingVertex = false;
    selectedEndingVertex = false;
    createVertices(); // Recreate the vertices
    theGraph.vertexList = new Array(); //reset the vertex list. So basically we are recreating the whole graph
    addVerticestoGraph(); //add vertices to the graph again
    theGraph.initializeadjMat(); //Create a new adj matrix
    createEdges(); //create new edges.
    theGraph.drawVertices();
    hasbeenreset = true; //update the flag
    theGraph.resetErasedEdges(); //Important for generating a new maze
    theGraph.mazeGenerated = false;//update the flag
}

function createVertices() { //Creates the 2d grid of vertices
    for (let i = 0; i < columns; i++) { //number of rows
        vertexgrid[i] = new Array(rows); //creating the 1D array of vertices
    }
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            vertexgrid[i][j] = new Vertex("X", i * (vertexLength+gap) + vertexOffset, (vertexLength+gap) * j + vertexOffset);
            //Each vertex in the array is then made into an array of vertices so we have a 2d array.
            vertexgrid[i][j].wasVisited = false;
        }
    }
}
function addVerticestoGraph() { //After the vertices have been created, we have to add it to the graph
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            theGraph.addVertex(vertexgrid[i][j]); //This function simply pushes these vertices into an arraylist
        }
    }
}


function createEdges() {

    for (let i = 0; i < rows; i++) {
        let marker2 = 1;
        while (marker2 < columns) {
            theGraph.addEdge(vertexgrid[marker2 - 1][i], vertexgrid[marker2][i]); //edges are created by denoting the value 1 in the adj matrix
            marker2++;
        }
    }

    for (let i = 0; i < columns; i++) {
        let marker2 = 1;
        while (marker2 < rows) {
            theGraph.addEdge(vertexgrid[i][marker2 - 1], vertexgrid[i][marker2]); //The same edge, but in the other direction. It is an undirected graph
            marker2++;
        }
    }
}





canvas.addEventListener("mousedown", async function (e) {
    if (theGraph.searching == true) {
        return;
    }
    mouseIsDown = true;
    let x = e.clientX - 10; //Getting the x and y coordinates of the mouse when clicked
    let y = e.clientY - 70; //
    if (selectedStartingVertex == false) { //If the start index hasn't been selected, we select it now
        for (let i = 0; i < columns; i++) { // for i = 0:(20, 60), i = 1:(60:100)
            for (let j = 0; j < rows; j++) {
                if (x > vertexOffset + i * (vertexLength+gap) && x < vertexOffset + (i + 1) * (vertexLength+gap) && y > vertexOffset + j * (vertexLength+gap) && y < vertexOffset + (j + 1) * (vertexLength+gap)) {
                    vertexgrid[i][j].startVertex = true;
                    selectedStartingVertex = true;
                    theGraph.drawVertices(); // After the vertex has been selected, we redraw the whole graph to show the difference
                    return;
                }
            }

        }
    }

    if (selectedEndingVertex == false) { //If the ending vertex hasn't been selected yet, we select one now. It has to be after the start index
        for (let i = 0; i < columns; i++) { // for i = 0:(20, 60), i = 1:(60:100)
            for (let j = 0; j < rows; j++) {
                if (x > vertexOffset + i * (vertexLength+gap) && x < vertexOffset + (i + 1) * (vertexLength+gap) && y > vertexOffset + j * (vertexLength+gap) && y < vertexOffset + (j + 1) * (vertexLength+gap)) {
                    vertexgrid[i][j].endVertex = true;
                    selectedEndingVertex = true;
                    theGraph.drawVertices(); //redraw the graph to show the difference
                    return;
                }
            }

        }
    }
    theGraph.drawVertices();
});



canvas.addEventListener("mouseup", function () {
    mouseIsDown = false; //A flag needed in the function to disable vertices
})

canvas.addEventListener("mousemove", function (e) { //This one is to disable vertices
    if (theGraph.searching == true) {
        return;
    }
    let x = e.clientX - 10;
    let y = e.clientY - 70;
    if (x <= 0 || y <= 0 || x >= 1300 || y >= 600) {
        //we don't want to be able to click outside the canvas and then drag into the canvas and disable vertices
        mouseIsDown = false;
    }
    if (mouseIsDown == true) { //When we click + drag the mouse


        for (let i = 0; i < columns; i++) { // for i = 0:(20, 60), i = 1:(60:100)
            for (let j = 0; j < rows; j++) {
                if (x > vertexOffset + i * (vertexLength+gap) && x < vertexOffset + (i + 1) * (vertexLength+gap) && y > vertexOffset + j * (vertexLength+gap) && y < vertexOffset + (j + 1) * (vertexLength+gap)) {
                    theGraph.disableVertex(vertexgrid[i][j]); //disable the vertex
                    break;
                }
            }

        }
        theGraph.drawVertices(); //redraw to show the disabled vertices
    }
});

async function iterativedfsbutton() { //button function
    if (selectedStartingVertex == false) { //flag. Shouldn't start a search if the start point hasn't been selected
        console.log("Please select a starting vertex");
        return;
    }
    if (theGraph.searching == true) { //Don't start a search if one is already on
        console.log("already searching");
        return;
    }

    if(hasbeenreset == false){ //Don't start a search unless the graph has been reset.
        return;
    }

    theGraph.iterativedfs(getstartingvertex());//Search with the selected start point
}

async function iterativebfsbutton(){ //The function to call when the bfs button is pressed.
    if (selectedStartingVertex == false) { //Don't start the search unless the start point has been selected
        console.log("Please select a starting vertex");
        return;
    }
    if (theGraph.searching == true) {//if a search is on, don't start another one
        console.log("already searching");
        return;
    }
    if(hasbeenreset == false){ //DOn't search unless the grid has been reset
        return;
    }
    theGraph.iterativeBFS(getstartingvertex()); //Use the start index to to start the search
}

async function shortestpathbutton(){ //The function to call when the SP is called
    if (selectedStartingVertex == false) { //Don't start the search unless the start point has been selected
        console.log("Please select a starting vertex");
        return;
    }
    if(selectedEndingVertex == false){ //Don't start the search unless the end point has been selected
        console.log("End vertex hasn't been selected");
        return;
    }
    if (theGraph.searching == true) {//Don't start the search if another one is still searching
        console.log("already searching");
        return;
    }

    if(hasbeenreset == false){ //Don't start the search unless the graph has been reset
        return;
    }

    theGraph.shortestpath(getstartingvertex(), getEndingVertex()); //Search from the selected start point to the selected end point
}

async function aStarSearchbutton(){
    if (selectedStartingVertex == false) { // don't search if the starting index hasn't been selected
        console.log("Please select a starting vertex");
        return;
    }
    if(selectedEndingVertex == false){ //don't start if the end vertex hasn't been selected
        console.log("Select the ending vertex");
        return;
    }
    if (theGraph.searching == true) { // Don't start if another search is already on
        console.log("already searching");
        return;
    }

    theGraph.aStarSearch(getstartingvertex(), getEndingVertex());
}

async function iterativeMazeGeneratorbutton(){ //Function to call when the MG button is pressed
    if(selectedStartingVertex == false){ // don't start generating without a startpoint
        console.log("Select the starting vertex");
        return;
    }
    if (theGraph.searching == true) { //Don't start if another search is already on
        console.log("already generating");
        return;
    }
    if(theGraph.mazeGenerated == true){ //Don't generate a maze if one has already been generated
        console.log("Maze has already been generated");
        return;
    }
    theGraph.dFSMazeGenerator(getstartingvertex());
}

function primsMazeGeneratorButton(){
    theGraph.primsMazeGenerator(getstartingvertex()); //NOT BEEN IMPLEMENTED
}


////////////////////////////////-------------------------FUNCTIONS ARE EXECUTED WHEN THE SCRIPT IS CALLED------------------------------////////////////////////////

alert("You can also click on the button to take you to another page about sorting algorithms");
alert("The graph algorithms all need a start point. Click on any vertex to select the start point. It will be coloured yellow. " +
    "This includes the maze generator (MG). " +
    "If you want to use the path finding algorithms, you will also need an ending point. Click again on any point to select it as the ending point. It will be coloured purple." +
    "You can also disable vertices. This can only be done after the start and end points have been selected. To disable vertices, just click the mouse button down and " +
    "continue moving over vertices to disable them. Make sure there is a path from the start point to the end point. The searches will also work with generated mazes. Use the slider to " +
    "change the bias of the A* algorithm. A higher bias will find the endpoint sooner but it will come at the cost of the length of the path. ")

createVertices(); // all good for reset
addVerticestoGraph();
theGraph.initializeadjMat();
createEdges();
theGraph.drawVertices();
graphreset();
